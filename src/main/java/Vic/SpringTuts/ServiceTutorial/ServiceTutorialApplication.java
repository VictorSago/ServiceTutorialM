package Vic.SpringTuts.ServiceTutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceTutorialApplication.class, args);
	}
}
