### Tutorial

## Building REST services with Spring

Following the Spring REST [tutorial](https://spring.io/guides/tutorials/bookmarks/). 

The complete code for the whole tutorial is on [gitHub](https://github.com/joshlong/bookmarks/tree/tutorial)
